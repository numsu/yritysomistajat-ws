### Setup ###

1 Install MariaDB (10.1.21)

2 Create an empty database

```
#!mysql
CREATE DATABASE yritysomistajat;
```

3 Get the project and its dependencies

```
#!bash
git clone https://numsu@bitbucket.org/numsu/yritysomistajat-ws.git
```

4 Set your database credentials in application.properties

5 Run the app

```
#!bash
mvn install
mvn spring-boot:run
```