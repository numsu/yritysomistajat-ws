package fi.numsu.yritysomistajatws;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import fi.numsu.yritysomistajatws.api.Company;
import fi.numsu.yritysomistajatws.service.CompanyWS;
import junit.framework.TestCase;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations="classpath:application-test.properties")
@DirtiesContext(classMode = ClassMode.BEFORE_CLASS)
public class CompanyWSTest {

	@Autowired
	private CompanyWS ws;

	@Test
	public void testCompany() {
		Company company = new Company();
		company.setName("Company");
		company.setIdentification("123456ab");
		company.setLand("fi");

		Company saved = ws.saveCompany(company);
		TestCase.assertNotNull(saved.getId());

		Company got = ws.getCompany(saved.getId());
		TestCase.assertEquals("Company", got.getName());
		TestCase.assertEquals("123456ab", got.getIdentification());
		TestCase.assertEquals("fi", got.getLand());

		got.setName("Changed company");
		ws.saveCompany(got);

		Company changed = ws.getCompany(got.getId());
		TestCase.assertEquals("Changed company", changed.getName());
		TestCase.assertEquals(got.getId(), changed.getId());

		Company company2 = new Company();
		company2.setName("Company");
		company2.setIdentification("654432ab");
		company2.setLand("fi");
		ws.saveCompany(company2);
		List<Company> companies = ws.getCompanies();
		TestCase.assertEquals(2, companies.size());

		changed.setDelete(true);
		ws.saveCompany(changed);
		Company deleted = ws.getCompany(changed.getId());
		TestCase.assertNull(deleted);
	}

}
