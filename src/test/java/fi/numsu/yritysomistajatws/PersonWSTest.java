package fi.numsu.yritysomistajatws;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import fi.numsu.yritysomistajatws.api.Person;
import fi.numsu.yritysomistajatws.service.PersonWS;
import junit.framework.TestCase;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations="classpath:application-test.properties")
@DirtiesContext(classMode = ClassMode.BEFORE_CLASS)
public class PersonWSTest {

	@Autowired
	private PersonWS ws;

	@Test
	public void testPerson() {
		Person person = new Person();
		person.setFirstname("Testi");
		person.setName("Testinen");
		person.setIdentification("241002-259D");
		person.setLand("fi");

		Person saved = ws.savePerson(person);
		TestCase.assertNotNull(saved.getId());

		Person got = ws.getPerson(saved.getId());
		TestCase.assertEquals(saved.getId(), got.getId());
		TestCase.assertEquals("Testi", got.getFirstname());

		got.setIdentification("010213-1127");
		got.setFirstname("Testitär");

		Person changed = ws.savePerson(got);
		TestCase.assertEquals("010213-1127", changed.getIdentification());
		TestCase.assertEquals("Testitär", changed.getFirstname());
		TestCase.assertEquals(got.getId(), changed.getId());

		Person person2 = new Person();
		person2.setFirstname("Testi2");
		person2.setName("Testinen2");
		person2.setIdentification("241002-258D");
		person2.setLand("se");
		ws.savePerson(person2);

		List<Person> persons = ws.getPersons();
		TestCase.assertEquals(2, persons.size());

		changed.setDelete(true);
		ws.savePerson(changed);

		Person poistettu = ws.getPerson(saved.getId());
		TestCase.assertNull(poistettu);
	}

}
