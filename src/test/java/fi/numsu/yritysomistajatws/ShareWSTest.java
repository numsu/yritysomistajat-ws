package fi.numsu.yritysomistajatws;

import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import fi.numsu.yritysomistajatws.api.Company;
import fi.numsu.yritysomistajatws.api.Person;
import fi.numsu.yritysomistajatws.api.Share;
import fi.numsu.yritysomistajatws.api.Shareholder;
import fi.numsu.yritysomistajatws.service.CompanyWS;
import fi.numsu.yritysomistajatws.service.PersonWS;
import fi.numsu.yritysomistajatws.service.ShareWS;
import junit.framework.TestCase;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations="classpath:application-test.properties")
@DirtiesContext(classMode = ClassMode.BEFORE_CLASS)
public class ShareWSTest {

	@Autowired
	private CompanyWS compWS;

	@Autowired
	private PersonWS persWS;

	@Autowired
	private ShareWS sharWS;

	private Company ownedCompany;
	private Company ownerCompany;
	private Person ownerPerson;

	@Before
	public void createData() {
		Company company = new Company();
		company.setIdentification("6141726-4");
		company.setLand("FI");
		company.setName("Company");
		ownedCompany = compWS.saveCompany(company);

		Company company2 = new Company();
		company.setIdentification("3624457-6");
		company.setLand("FI");
		company.setName("Companyowner");
		ownerCompany = compWS.saveCompany(company2);

		Person person = new Person();
		person.setFirstname("Test");
		person.setName("Tester");
		person.setBirthdate(new Date());
		person.setIdentification("080777-095R");
		person.setLand("FI");
		person.setTaxcode("1234asd");
		ownerPerson = persWS.savePerson(person);
	}

	@Test
	public void testCompanyShareholder() {
		Share share = new Share();
		share.setCompany(ownedCompany);
		Shareholder holder = new Shareholder();
		holder.setId(ownerCompany.getId());
		holder.setType(1);
		share.setShareholder(holder);
		share.setHolding(15.6);
		share = sharWS.saveShare(share);

		Company ownedComp = compWS.getCompany(share.getCompany().getId());
		TestCase.assertEquals(true, ownedComp.getShares().stream().anyMatch(s -> s.getShareholder().getId() == ownerCompany.getId()));
	}

	@Test
	public void testPersonShareholder() {
		Share share = new Share();
		share.setCompany(ownedCompany);
		Shareholder holder = new Shareholder();
		holder.setId(ownerPerson.getId());
		holder.setType(2);
		share.setShareholder(holder);
		share.setHolding(20.74);
		share = sharWS.saveShare(share);

		Company ownedComp = compWS.getCompany(share.getCompany().getId());
		TestCase.assertEquals(true, ownedComp.getShares().stream().anyMatch(s -> s.getShareholder().getId() == ownerPerson.getId()));

		share.setDelete(true);
		sharWS.saveShare(share);
		TestCase.assertNull(sharWS.getShare(share.getId()));
	}

	@After
	public void deleteData() {
		ownedCompany.setDelete(true);
		compWS.saveCompany(ownedCompany);

		ownerCompany.setDelete(true);
		compWS.saveCompany(ownerCompany);

		ownerPerson.setDelete(true);
		persWS.savePerson(ownerPerson);
	}

}
