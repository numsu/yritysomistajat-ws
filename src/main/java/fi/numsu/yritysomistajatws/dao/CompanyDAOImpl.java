package fi.numsu.yritysomistajatws.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import fi.numsu.yritysomistajatws.hbm.HCompany;

@Repository
public class CompanyDAOImpl implements CompanyDAO {

    private final SessionFactory sessionFactory;

    @Autowired
    public CompanyDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

	@Override
	@SuppressWarnings("unchecked")
	public List<HCompany> getAllCompanies() {
		List<HCompany> res = sessionFactory.getCurrentSession().createQuery("from HCompany").list();
		return res;
	}

}
