package fi.numsu.yritysomistajatws.dao;

import java.util.List;

import fi.numsu.yritysomistajatws.hbm.HCompany;

public interface CompanyDAO {

	List<HCompany> getAllCompanies();

}
