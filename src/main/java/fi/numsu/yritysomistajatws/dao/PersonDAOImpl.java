package fi.numsu.yritysomistajatws.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import fi.numsu.yritysomistajatws.hbm.HPerson;

@Repository
public class PersonDAOImpl implements PersonDAO {

    private final SessionFactory sessionFactory;

    @Autowired
    public PersonDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

	@Override
	@SuppressWarnings("unchecked")
	public List<HPerson> getAllPersons() {
		List<HPerson> res = sessionFactory.getCurrentSession().createQuery("from HPerson").list();
		return res;
	}

}
