package fi.numsu.yritysomistajatws.dao;

import java.util.List;

import fi.numsu.yritysomistajatws.hbm.HPerson;

public interface PersonDAO {

	List<HPerson> getAllPersons();

}
