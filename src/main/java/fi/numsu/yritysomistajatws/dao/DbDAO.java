package fi.numsu.yritysomistajatws.dao;

import fi.numsu.yritysomistajatws.hbm.HBase;

public interface DbDAO {

	void insert(HBase obj);
	void update(HBase obj);
	void delete(HBase obj);
	<T extends HBase> T get(Class<T> name, int avain);

}
