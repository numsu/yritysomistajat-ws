package fi.numsu.yritysomistajatws.dao;

import java.util.Date;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import fi.numsu.yritysomistajatws.hbm.HBase;

@Repository
public class DbDAOImpl implements DbDAO {

    private final SessionFactory sessionFactory;

    @Autowired
    public DbDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

	@Override
	public <T extends HBase> T get(Class<T> name, int avain) {
		return sessionFactory.getCurrentSession().get(name, avain);
	}

	@Override
	public void insert(HBase obj) {
		obj.setSaved(new Date());
		obj.setSaver("TODO");
		obj.setModified(new Date());
		obj.setModifier("TODO");
		sessionFactory.getCurrentSession().save(obj);
	}

	@Override
	public void update(HBase obj) {
		obj.setModified(new Date());
		obj.setModifier("TODO");
		sessionFactory.getCurrentSession().update(obj);
	}

	@Override
	public void delete(HBase obj) {
		sessionFactory.getCurrentSession().delete(obj);
	}

}
