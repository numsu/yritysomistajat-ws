package fi.numsu.yritysomistajatws.api;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import fi.numsu.yritysomistajatws.hbm.HCompany;

public class Company extends WebUIObject {

	private int id;
	private String name;
	private String identification;
	private String land;

	private List<Share> shareholders = new ArrayList<>();

	public Company() {
	}

	public Company(HCompany company) {
		this.id = company.getId();
		this.name = company.getName();
		this.identification = company.getIdentification();
		this.land = company.getLand();
		this.shareholders = company.getHolders().stream().map(Share::new).collect(Collectors.toList());
	}

	public Company(int id, String name, String identification, String land, List<Share> shares) {
		this.id = id;
		this.name = name;
		this.identification = identification;
		this.land = land;
		this.shareholders = shares;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIdentification() {
		return identification;
	}

	public void setIdentification(String identification) {
		this.identification = identification;
	}

	public String getLand() {
		return land;
	}

	public void setLand(String land) {
		this.land = land;
	}

	public List<Share> getShares() {
		return shareholders;
	}

	public void setShares(List<Share> shares) {
		this.shareholders = shares;
	}

}
