package fi.numsu.yritysomistajatws.api;

import fi.numsu.yritysomistajatws.hbm.HCompany;
import fi.numsu.yritysomistajatws.hbm.HPerson;

public class Shareholder {

	private int id;
	private String name;
	private String identification;
	private String land;
	private int type;

	public Shareholder() {
	}

	public Shareholder(int id, String name, String firstname, String identification, String land, int type) {
		this.id = id;
		if (type == 2)
			this.name = firstname + ' ' + name;
		else
			this.name = name;
		this.identification = identification;
		this.land = land;
		this.type = type;
	}

	public Shareholder(HCompany com) {
		this.id = com.getId();
		this.name = com.getName();
		this.identification = com.getIdentification();
		this.land = com.getLand();
		this.type = 1;
	}

	public Shareholder(HPerson per) {
		this.id = per.getId();
		this.name = per.getFullName();
		this.identification = per.getIdentification();
		this.land = per.getLand();
		this.type = 2;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIdentification() {
		return identification;
	}

	public void setIdentification(String identification) {
		this.identification = identification;
	}

	public String getLand() {
		return land;
	}

	public void setLand(String land) {
		this.land = land;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

}
