package fi.numsu.yritysomistajatws.api;

import java.util.Date;

import fi.numsu.yritysomistajatws.hbm.HPerson;

public class Person extends WebUIObject {

	private int id;
	private String name;
	private String firstname;
	private String fullname;
	private String identification;
	private Date birthdate;
	private String land;
	private String taxcode;

	public Person() {
	}

	public Person(HPerson per) {
		this.id = per.getId();
		this.name = per.getName();
		this.firstname = per.getFirstname();
		this.fullname = this.firstname + " " + this.name;
		this.identification = per.getIdentification();
		this.birthdate = per.getBirthdate();
		this.land = per.getLand();
		this.taxcode = per.getTaxcode();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getIdentification() {
		return identification;
	}

	public void setIdentification(String identification) {
		this.identification = identification;
	}

	public Date getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	public String getLand() {
		return land;
	}

	public void setLand(String land) {
		this.land = land;
	}

	public String getTaxcode() {
		return taxcode;
	}

	public void setTaxcode(String taxcode) {
		this.taxcode = taxcode;
	}

}
