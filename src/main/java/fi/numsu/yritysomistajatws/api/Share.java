package fi.numsu.yritysomistajatws.api;

import fi.numsu.yritysomistajatws.hbm.HShare;

public class Share extends WebUIObject {

	private int id;
	private Company company;
	private Shareholder shareholder;
	private double holding;

	public Share() {
	}

	public Share(HShare share) {
		this.id = share.getId();
		if (share.getCompanyHolder() != null)
			this.shareholder = new Shareholder(share.getCompanyHolder());
		else
			this.shareholder = new Shareholder(share.getPersonHolder());
		this.holding = share.getHolding();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Shareholder getShareholder() {
		return shareholder;
	}

	public void setShareholder(Shareholder shareholder) {
		this.shareholder = shareholder;
	}

	public double getHolding() {
		return holding;
	}

	public void setHolding(double holding) {
		this.holding = holding;
	}

}
