package fi.numsu.yritysomistajatws.api;

public class WebUIObject {

	private boolean delete;

	public boolean isDelete() {
		return delete;
	}

	public void setDelete(boolean delete) {
		this.delete = delete;
	}

}
