package fi.numsu.yritysomistajatws.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fi.numsu.yritysomistajatws.api.Person;
import fi.numsu.yritysomistajatws.dao.DbDAO;
import fi.numsu.yritysomistajatws.dao.PersonDAO;
import fi.numsu.yritysomistajatws.hbm.HPerson;
import fi.numsu.yritysomistajatws.hbm.HShare;

@RestController
@RequestMapping("/person")
public class PersonWSImpl implements PersonWS {

	@Autowired
	private DbDAO dbDAO;

	@Autowired
	private PersonDAO personDAO;

	@Override
	@Transactional
	@RequestMapping("/save")
	public Person savePerson(@RequestBody Person person) {
		if (person.isDelete()) {
			HPerson per = dbDAO.get(HPerson.class, person.getId());
			for (HShare share : per.getShares()) {
				dbDAO.delete(share);
			}
			dbDAO.delete(per);
			return new Person();
		}

		HPerson per;
		if (person.getId() != 0) {
			per = dbDAO.get(HPerson.class, person.getId());
		} else {
			per = new HPerson();
		}

		per.setName(person.getName());
		per.setFirstname(person.getFirstname());
		per.setIdentification(person.getIdentification());
		per.setLand(person.getLand());
		per.setBirthdate(person.getBirthdate());
		per.setTaxcode(person.getTaxcode());

		if (person.getId() != 0) {
			dbDAO.update(per);
		} else {
			dbDAO.insert(per);
			person.setId(per.getId());
		}

		return person;
	}

	@Override
	@Transactional
	@RequestMapping("/get/{id}")
	public Person getPerson(@PathVariable int id) {
		HPerson person = dbDAO.get(HPerson.class, id);
		return person != null ? new Person(person) : null;
	}


	@Override
	@Transactional
	@RequestMapping("/getall")
	public List<Person> getPersons() {
		List<Person> res = new ArrayList<>();
		personDAO.getAllPersons().forEach(person -> res.add(new Person(person)));
		return res;
	}

}
