package fi.numsu.yritysomistajatws.service;

import java.util.List;

import fi.numsu.yritysomistajatws.api.Company;

public interface CompanyWS {

	Company saveCompany(Company company);

	Company getCompany(Integer id);

	List<Company> getCompanies();

}
