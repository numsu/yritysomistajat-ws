package fi.numsu.yritysomistajatws.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fi.numsu.yritysomistajatws.api.Company;
import fi.numsu.yritysomistajatws.dao.CompanyDAO;
import fi.numsu.yritysomistajatws.dao.DbDAO;
import fi.numsu.yritysomistajatws.hbm.HCompany;
import fi.numsu.yritysomistajatws.hbm.HShare;

@RestController
@RequestMapping("/company")
public class CompanyWSImpl implements CompanyWS {

	@Autowired
	private DbDAO dbDAO;

	@Autowired
	private CompanyDAO companyDAO;

	@Override
	@Transactional
	@RequestMapping("/save")
	public Company saveCompany(@RequestBody Company company) {
		if (company.isDelete()) {
			HCompany comp = dbDAO.get(HCompany.class, company.getId());
			for (HShare share : comp.getShares()) {
				dbDAO.delete(share);
			}
			for (HShare share : comp.getHolders()) {
				dbDAO.delete(share);
			}
			dbDAO.delete(comp);
			return new Company();
		}

		HCompany comp;
		if (company.getId() != 0) {
			comp = dbDAO.get(HCompany.class, company.getId());
		} else {
			comp = new HCompany();
		}

		comp.setName(company.getName());
		comp.setIdentification(company.getIdentification());
		comp.setLand(company.getLand());

		if (company.getId() != 0) {
			dbDAO.update(comp);
		} else {
			dbDAO.insert(comp);
			company.setId(comp.getId());
		}

		return company;
	}

	@Override
	@Transactional
	@RequestMapping("/get/{id}")
	public Company getCompany(@PathVariable Integer id) {
		HCompany company = dbDAO.get(HCompany.class, id);
		return company != null ? new Company(company) : null;
	}

	@Override
	@Transactional
	@RequestMapping("/getall")
	public List<Company> getCompanies() {
		List<Company> res = new ArrayList<>();
		companyDAO.getAllCompanies().forEach(company -> res.add(new Company(company)));
		return res;
	}

}
