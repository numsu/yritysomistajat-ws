package fi.numsu.yritysomistajatws.service;

import java.util.List;

import fi.numsu.yritysomistajatws.api.Person;

public interface PersonWS {

	Person savePerson(Person person);

	Person getPerson(int id);

	List<Person> getPersons();

}
