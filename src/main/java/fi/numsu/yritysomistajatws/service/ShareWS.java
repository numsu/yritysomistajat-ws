package fi.numsu.yritysomistajatws.service;

import fi.numsu.yritysomistajatws.api.Share;

public interface ShareWS {

	public Share saveShare(Share share);

	public Share getShare(int id);

}
