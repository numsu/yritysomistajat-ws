package fi.numsu.yritysomistajatws.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fi.numsu.yritysomistajatws.api.Share;
import fi.numsu.yritysomistajatws.dao.DbDAO;
import fi.numsu.yritysomistajatws.hbm.HCompany;
import fi.numsu.yritysomistajatws.hbm.HPerson;
import fi.numsu.yritysomistajatws.hbm.HShare;

@RestController
@RequestMapping("/share")
public class ShareWSImpl implements ShareWS {

	@Autowired
	private DbDAO dbDAO;

	@Override
	@Transactional
	@RequestMapping("/save")
	public Share saveShare(@RequestBody Share share) {
		if (share.isDelete()) {
			HShare shr = dbDAO.get(HShare.class, share.getId());
			dbDAO.delete(shr);
			return new Share();
		}

		HShare shr;
		if (share.getId() != 0) {
			shr = dbDAO.get(HShare.class, share.getId());
		} else {
			shr = new HShare();
		}

		shr.setCompany(dbDAO.get(HCompany.class, share.getCompany().getId()));

		if (share.getShareholder().getType() == 1) {
			shr.setCompanyHolder(dbDAO.get(HCompany.class, share.getShareholder().getId()));
		} else {
			shr.setPersonHolder(dbDAO.get(HPerson.class, share.getShareholder().getId()));
		}

		shr.setHolding(share.getHolding());

		if (share.getId() != 0) {
			dbDAO.update(shr);
		} else {
			dbDAO.insert(shr);
			share.setId(shr.getId());
		}

		return share;
	}

	@Override
	@Transactional
	@RequestMapping("/get/{id}")
	public Share getShare(@PathVariable int id) {
		HShare share = dbDAO.get(HShare.class, id);
		return share != null ? new Share(share) : null;
	}

}
