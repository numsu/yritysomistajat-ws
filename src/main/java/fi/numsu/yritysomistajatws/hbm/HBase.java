package fi.numsu.yritysomistajatws.hbm;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
@SuppressWarnings("serial")
public class HBase implements Serializable {

	private Date saved;
	private Date modified;
	private String modifier;
	private String saver;

	public Date getSaved() {
		return saved;
	}

	public void setSaved(Date saved) {
		this.saved = saved;
	}

	public Date getModified() {
		return modified;
	}

	public void setModified(Date modified) {
		this.modified = modified;
	}

	public String getModifier() {
		return modifier;
	}

	public void setModifier(String modifier) {
		this.modifier = modifier;
	}

	public String getSaver() {
		return saver;
	}

	public void setSaver(String saver) {
		this.saver = saver;
	}

}
