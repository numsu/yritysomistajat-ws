package fi.numsu.yritysomistajatws.hbm;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "PERSON")
public class HPerson extends HBase implements IOwner {
	private static final long serialVersionUID = 1945777067465462170L;

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private int id;

	@Column(name = "NAME")
	private String name;

	@Column(name = "FIRST_NAME")
	private String firstname;

	@Column(name = "IDENTIFICATION")
	private String identification;

	@Column(name = "BIRTHDATE")
	private Date birthdate;

	@Column(name = "LAND")
	private String land;

	@Column(name = "TAX_CODE")
	private String taxcode;


	@OneToMany(fetch = FetchType.LAZY, mappedBy = "personHolder")
	private Set<HShare> shares = new HashSet<>();

	public HPerson() {
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getFullName() {
		return this.firstname + " " + this.name;
	}

	@Override
	public String getIdentification() {
		return identification;
	}

	@Override
	public void setIdentification(String identification) {
		this.identification = identification;
	}

	public Date getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	@Override
	public String getLand() {
		return land;
	}

	@Override
	public void setLand(String land) {
		this.land = land;
	}

	public String getTaxcode() {
		return taxcode;
	}

	public void setTaxcode(String taxcode) {
		this.taxcode = taxcode;
	}

	public Set<HShare> getShares() {
		return this.shares;
	}

}
