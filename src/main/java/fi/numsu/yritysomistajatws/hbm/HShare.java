package fi.numsu.yritysomistajatws.hbm;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "SHARE")
public class HShare extends HBase {
	private static final long serialVersionUID = 1945777067465462170L;

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private int id;

	@JoinColumn(name = "COMPANY_ID")
	@ManyToOne
	private HCompany company;

	@JoinColumn(name = "COMPANY_HOLDER_ID")
	@ManyToOne
	private HCompany companyHolder;

	@JoinColumn(name = "PERSON_HOLDER_ID")
	@ManyToOne
	private HPerson personHolder;

	@Column(name = "HOLDING")
	private double holding;

	public HShare() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public HCompany getCompany() {
		return company;
	}

	public void setCompany(HCompany company) {
		this.company = company;
	}

	public HCompany getCompanyHolder() {
		return companyHolder;
	}

	public void setCompanyHolder(HCompany companyHolder) {
		this.companyHolder = companyHolder;
	}

	public HPerson getPersonHolder() {
		return personHolder;
	}

	public void setPersonHolder(HPerson personHolder) {
		this.personHolder = personHolder;
	}

	public double getHolding() {
		return holding;
	}

	public void setHolding(double holding) {
		this.holding = holding;
	}

}
