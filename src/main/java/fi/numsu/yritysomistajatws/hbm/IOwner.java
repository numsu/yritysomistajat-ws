package fi.numsu.yritysomistajatws.hbm;

public interface IOwner {

	public int getId();

	public void setId(int id);

	public String getName();

	public void setName(String name);

	public String getIdentification();

	public void setIdentification(String identification);

	public String getLand();

	public void setLand(String land);

}
