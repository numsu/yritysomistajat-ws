package fi.numsu.yritysomistajatws.hbm;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "COMPANY")
public class HCompany extends HBase implements IOwner {
	private static final long serialVersionUID = 1945777067465462170L;

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private int id;

	@Column(name = "NAME")
	private String name;

	@Column(name = "IDENTIFICATION")
	private String identification;

	@Column(name = "LAND")
	private String land;


	@OneToMany(fetch = FetchType.LAZY, mappedBy = "companyHolder")
	private Set<HShare> shares = new HashSet<>();

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "company")
	private Set<HShare> holders = new HashSet<>();

	public HCompany() {
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getIdentification() {
		return identification;
	}

	@Override
	public void setIdentification(String identification) {
		this.identification = identification;
	}

	@Override
	public String getLand() {
		return land;
	}

	@Override
	public void setLand(String land) {
		this.land = land;
	}

	public Set<HShare> getShares() {
		return shares;
	}

	public Set<HShare> getHolders() {
		return holders;
	}

}
