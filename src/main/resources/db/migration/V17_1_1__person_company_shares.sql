create table PERSON (
	SAVED timestamp not null,
	MODIFIED timestamp not null,
	MODIFIER varchar(30) not null,
	SAVER varchar(30) not null,
	ID int not null AUTO_INCREMENT,
	NAME varchar(100) not null,
	FIRST_NAME varchar(100),
	IDENTIFICATION varchar(20),
	BIRTHDATE date,
	LAND varchar(2),
	TAX_CODE varchar(40),
	PRIMARY KEY (ID)
) ENGINE=InnoDB;

create table COMPANY (
	SAVED timestamp not null,
	MODIFIED timestamp not null,
	MODIFIER varchar(30) not null,
	SAVER varchar(30) not null,
	ID int not null AUTO_INCREMENT,
	NAME varchar(100) not null,
	IDENTIFICATION varchar(20),
	LAND varchar(2),
	PRIMARY KEY (ID)
) ENGINE=InnoDB;

create table SHARE (
	SAVED timestamp not null,
	MODIFIED timestamp not null,
	MODIFIER varchar(30) not null,
	SAVER varchar(30) not null,
	ID int not null AUTO_INCREMENT,
	COMPANY_ID int not null,
	COMPANY_HOLDER_ID int,
	PERSON_HOLDER_ID int,
	HOLDING double not null,
	PRIMARY KEY (ID),
	FOREIGN KEY (COMPANY_ID) REFERENCES COMPANY(ID),
	FOREIGN KEY (COMPANY_HOLDER_ID) REFERENCES COMPANY(ID),
	FOREIGN KEY (PERSON_HOLDER_ID) REFERENCES PERSON(ID)
) ENGINE=InnoDB;
